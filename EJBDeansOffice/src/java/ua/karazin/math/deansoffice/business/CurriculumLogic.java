/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.karazin.math.deansoffice.business;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import ua.karazin.math.deansoffice.db.dao.EducationDirectionDAO;
import ua.karazin.math.deansoffice.db.dao.EducationLevelDAO;
import ua.karazin.math.deansoffice.db.dao.EducationPlanDAO;
import ua.karazin.math.deansoffice.db.dao.EducationPlanSubjectDAO;
import ua.karazin.math.deansoffice.db.dao.EducationSpecialityDAO;
import ua.karazin.math.deansoffice.db.dao.SubjectDAO;
import ua.karazin.math.deansoffice.db.dao.TeacherDAO;
import ua.karazin.math.deansoffice.db.model.EducationDirection;
import ua.karazin.math.deansoffice.db.model.EducationLevel;
import ua.karazin.math.deansoffice.db.model.EducationPlan;
import ua.karazin.math.deansoffice.db.model.EducationPlanEntry;
import ua.karazin.math.deansoffice.db.model.EducationPlanSubject;
import ua.karazin.math.deansoffice.db.model.EducationSpeciality;
import ua.karazin.math.deansoffice.db.model.Subject;   // entity
import ua.karazin.math.deansoffice.db.model.Teacher;
import ua.karazin.math.deansoffice.db.model.TeacherSubject;
import ua.karazin.math.deansoffice.view.Curriculum.EducationDirectionView;
import ua.karazin.math.deansoffice.view.Curriculum.EducationLevelView;
import ua.karazin.math.deansoffice.view.Curriculum.EducationPlanView;
import ua.karazin.math.deansoffice.view.Curriculum.EducationPlanEntryView;
import ua.karazin.math.deansoffice.view.Curriculum.EducationPlanInfoView;
import ua.karazin.math.deansoffice.view.Curriculum.EducationSpecialityView;
import ua.karazin.math.deansoffice.view.Subject.ShortSubjectView;

/**
 *
 * @author Alyona
 */
@Stateless
@LocalBean
public class CurriculumLogic {
    @EJB
    private TeacherDAO teacherDAO;
    @EJB
    private EducationDirectionDAO edirectionDAO;
    @EJB
    private EducationSpecialityDAO specialityDAO;
    @EJB
    private EducationPlanDAO planDAO;
    @EJB
    private EducationLevelDAO levelDAO;
    @EJB
    private SubjectDAO subjectDAO; // подключаем менеджер
   // @EJB
    //private EducationPlanDAO educationPlanDAO;
    
   
     public EducationPlanInfoView getEducationPlanInfo(long idEducationPlan){
         EducationPlan educationPlan = planDAO.find(idEducationPlan);
         EducationPlanInfoView educationPlanInfoView = new EducationPlanInfoView();
         educationPlanInfoView.setAcademicRights(educationPlan.getAcademicRights());
         educationPlanInfoView.setProfRights(educationPlan.getProfRights());
         educationPlanInfoView.setSemesterCount(educationPlan.getSemesterCount());
         educationPlanInfoView.setQualification(educationPlan.getQualification());
         educationPlanInfoView.setDirection(educationPlan.getEducationDirection().getName());
         educationPlanInfoView.setForm(educationPlan.getEducationForm().getName());
         educationPlanInfoView.setLevel(educationPlan.getEducationLevel().getName());
         educationPlanInfoView.setSpeciality(educationPlan.getEducationSpeciality().getName());
         return educationPlanInfoView;
     }
    
    public List<EducationPlanEntryView> getEducationPlanEntryList(long idEducationPlan){
        
        EducationPlan educationPlan = planDAO.find(idEducationPlan); 
        List <EducationPlanSubject>educationPlanSubjectList;
         List<EducationPlanEntry> educationPlanEntryList = new ArrayList();
        List<EducationPlanEntryView> educationPlanEntryViewList = new ArrayList();
        
        if(educationPlan!=null){
            educationPlanSubjectList=educationPlan.getEducationPlanSubjectList();
            if(educationPlanSubjectList != null)
                for(EducationPlanSubject subject: educationPlanSubjectList){
                    educationPlanEntryList=subject.getSubject().getEducationPlanEntryList();
                    if(educationPlanEntryList!=null)
                        for(EducationPlanEntry entry: educationPlanEntryList){
                            EducationPlanEntryView educationPlanEntryView = new EducationPlanEntryView();
                            educationPlanEntryView.setId(entry.getId());
                            educationPlanEntryView.setHoursTotal(entry.getHoursTotal());
                            educationPlanEntryView.setHoursAuditor(entry.getHoursAuditor());
                            educationPlanEntryView.setIsMandatory(entry.getIsMandatory());
                            educationPlanEntryView.setNumber(entry.getNumber());
                            educationPlanEntryView.setName(entry.getName());
                            educationPlanEntryView.setSemestr(entry.getSemester());
                            educationPlanEntryView.setsCode(entry.getSCode());
                            educationPlanEntryView.setReportType(entry.getReportType().getName());     
                            educationPlanEntryView.setCountModules(entry.getUnitList().size());
                            educationPlanEntryViewList.add(educationPlanEntryView);
                        } 
                     }
            else educationPlanEntryViewList = null;
            }
        else educationPlanEntryViewList = null;
        
        return educationPlanEntryViewList;
            
    }
    
    
    public List<ShortSubjectView> getShortSubjectList(){
        List<Subject> SubjectList = subjectDAO.findAll(); // обьекты ентити
        List<ShortSubjectView> shortSubjectViewList = new ArrayList();
        if(SubjectList != null)
            for(Subject subject: SubjectList){
                ShortSubjectView shortSubjectView = new ShortSubjectView();
                shortSubjectView.setIdSubject(subject.getId());
                shortSubjectView.setNameSubject(subject.getName());
                shortSubjectViewList.add(shortSubjectView);
            }
        else{
            shortSubjectViewList = null;
        }
        return shortSubjectViewList;//дальше обновляем ws файл(удаляем ws и создаем заново)
    }
    public List<EducationSpecialityView> getEducationSpecialityList() {
        List<EducationSpeciality> educationSpecialityList = specialityDAO.findAll();//.getEducationSpecialityList();
        ArrayList<EducationSpecialityView> educationSpecialityViewList= new ArrayList<EducationSpecialityView>();
        if(educationSpecialityList!=null){
            //educationSpecialityViewList = new ArrayList<EducationSpecialityView>(educationSpecialityList.size());
            for (EducationSpeciality educationSpeciality: educationSpecialityList) {
                EducationSpecialityView educationSpecialityView = new EducationSpecialityView();
                //educationSpecialityView.setId(educationSpeciality.getId());
                educationSpecialityView.setName(educationSpeciality.getName());
                educationSpecialityView.setSCode(educationSpeciality.getSCode());
                educationSpecialityView.setVersion(educationSpeciality.getVersion());
              
                educationSpecialityViewList.add(educationSpecialityView);
            }
            
        }else educationSpecialityViewList=null;
        return educationSpecialityViewList;
    }
    
     /**
     * Формирует список учебных планов для указаного образовательного уровня и направления
     * @param idLevel - код образовательного уровня, 
     * @param idDirection - код образовательного направления
     * @return список учебных планов
     */
    public List<EducationPlanView> getEducationPlanList(int idLevel, int idDirection) {
        List<EducationPlan> educationPlanList = planDAO.findAll();//.getEducationPlanList(idLevel, idDirection);
        ArrayList<EducationPlanView> educationPlanViewList = new ArrayList<EducationPlanView>();//(plans.size());
        if(educationPlanList!=null){
            for (EducationPlan educationPlan: educationPlanList){
                EducationPlanView educationPlanView = new EducationPlanView();
               // educationPlanView.setId(educationPlan.getId());
                educationPlanView.setName(educationPlan.getName());
                educationPlanView.setSemesterCount(educationPlan.getSemesterCount());
                educationPlanView.setVersion(educationPlan.getVersion());
                educationPlanView.setNote(educationPlan.getNote());
                educationPlanView.setQualification(educationPlan.getQualification());
                educationPlanView.setProfRights(educationPlan.getProfRights());
                educationPlanView.setAcademicRights(educationPlan.getAcademicRights());
                
                educationPlanViewList.add(educationPlanView);
            }
        }else educationPlanViewList=null;
        return educationPlanViewList;
    }

      /**
     * Формирует список образовательных уровней
     *
     * @return список образовательных уровней
     */
    public List<EducationLevelView> getEducationLevelList() {
        List<EducationLevel> educationLevelList = levelDAO.findAll();//.getEducationLevelList();
        ArrayList<EducationLevelView> educationLevelViewList = new ArrayList<EducationLevelView>();//(educationLevelList.size());
        if(educationLevelList!=null){
            for (EducationLevel educationLevel: educationLevelList) {
                EducationLevelView educationLevelView = new EducationLevelView();
               // educationLevelView.setId(educationLevel.getId());
                educationLevelView.setName(educationLevel.getName());
                educationLevelView.setSCode(educationLevel.getSCode());
                educationLevelView.setVersion(educationLevel.getVersion());
                
                educationLevelViewList.add(educationLevelView);
            }
        }else educationLevelViewList=null;
        return educationLevelViewList;
    }

     /**
     * Формирует список образовательных направлений
     *
     * @return список образовательных направлений
     */
    public List<EducationDirectionView> getEducationDirectionList() {
        List<EducationDirection> educationDirectionList = edirectionDAO.findAll();
        ArrayList<EducationDirectionView> educationDirectionViewList = new ArrayList<EducationDirectionView>();//(directions.size());
        if(educationDirectionList!=null){
            for (EducationDirection educationDirection: educationDirectionList) {
                EducationDirectionView educationDirectionView = new EducationDirectionView();
               // educationDirectionView.setId(educationDirection.getId());
                educationDirectionView.setName(educationDirection.getName());
                educationDirectionView.setExtraPercent(educationDirection.getExtraPercent());
                educationDirectionView.setSCode(educationDirection.getSCode());
                educationDirectionViewList.add(educationDirectionView);
            }
        }else educationDirectionViewList=null;
        return educationDirectionViewList;
    }

     /**
     * Формирует список образовательных уровней
     * @param idTeacher - код преподавателя
     * @return список образовательных уровней
     */
    public List<ShortSubjectView> getTeacherSubjectList(Integer idTeacher) {
        ArrayList <ShortSubjectView> shortSubjectViewList=new ArrayList<ShortSubjectView>();
        Teacher teacher = teacherDAO.find(idTeacher);
        List <TeacherSubject> teacherSubjectList=teacher.getTeacherSubjectList();
        List<Subject> subjectList=new ArrayList<Subject>();
        if (teacherSubjectList!=null){
            for(TeacherSubject teacherSubject:teacherSubjectList){
                subjectList.add(teacherSubject.getSubject());
            }
            if(subjectList!=null){
                for (Subject subject : subjectList) {
                    ShortSubjectView shortSubjectView = new ShortSubjectView();
                    shortSubjectView.setIdSubject(subject.getId());
                    shortSubjectView.setNameSubject(subject.getName());
                    
                    
                }
            }
        }
        else {
            shortSubjectViewList=null;
        }
        return shortSubjectViewList;
    }

//    public List<ShortSubjectView> getTeacherDepartmentList(Integer id)// вывести предметы по id преподавателя
//    {
//        ArrayList <ShortSubjectView> infoList=new ArrayList<ShortSubjectView>();
//        Teacher teacher = teacherDAO.find(id);
//        List <TeacherSubject> teacherSubjectList=(List)teacher.getTeacherSubjectCollection();
//        List<Subject> subjectList=new ArrayList<Subject>();
//        if (teacherSubjectList!=null){
//            for(TeacherSubject teacherSubject:teacherSubjectList){
//                subjectList.add(teacherSubject.getSubject());
//            }
//            for (Subject subject : subjectList) {
//                ShortSubjectView info = new ShortSubjectView();
//                infoList.add(info);
//                info.setIdSubject(subject.getId());
//                info.setNameSubject(subject.getName());
//            }
//        }
//        else {
//            infoList=null;
//        }
//        return infoList;
//    }
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

}
