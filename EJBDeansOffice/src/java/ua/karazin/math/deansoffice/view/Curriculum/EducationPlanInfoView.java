/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ua.karazin.math.deansoffice.view.Curriculum;

/**
 *
 * @author DEN
 */
public class EducationPlanInfoView {
    
    private String direction;
    private String speciality;
    private String form;
    private String level;
    private String qualification;
    private String academicRights;
    private String profRights;
    private short semesterCount;
    
    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getAcademicRights() {
        return academicRights;
    }

    public void setAcademicRights(String academicRights) {
        this.academicRights = academicRights;
    }

    public String getProfRights() {
        return profRights;
    }

    public void setProfRights(String profRights) {
        this.profRights = profRights;
    }

    public short getSemesterCount() {
        return semesterCount;
    }

    public void setSemesterCount(short semesterCount) {
        this.semesterCount = semesterCount;
    }

    
}
