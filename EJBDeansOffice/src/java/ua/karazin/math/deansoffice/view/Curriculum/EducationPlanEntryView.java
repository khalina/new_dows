/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ua.karazin.math.deansoffice.view.Curriculum;

/**
 *
 * @author DEN
 */
public class EducationPlanEntryView {
    
    private long id;
    private String name;
    private int hoursTotal;
    private String number;
    private int hoursAuditor;
    private int semestr;
    private String reportType;
    private boolean isMandatory;
    private String sCode;
    private int countModules;
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getHoursTotal() {
        return hoursTotal;
    }

    public void setHoursTotal(int hoursTotal) {
        this.hoursTotal = hoursTotal;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public long getHoursAuditor() {
        return hoursAuditor;
    }

    public void setHoursAuditor(int hoursAuditor) {
        this.hoursAuditor = hoursAuditor;
    }

    public long getSemestr() {
        return semestr;
    }

    public void setSemestr(int semestr) {
        this.semestr = semestr;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public boolean isIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(boolean isMandatory) {
        this.isMandatory = isMandatory;
    }

    public String getsCode() {
        return sCode;
    }

    public void setsCode(String sCode) {
        this.sCode = sCode;
    }

    public long getCountModules() {
        return countModules;
    }

    public void setCountModules(int countModules) {
        this.countModules = countModules;
    }
    
}
