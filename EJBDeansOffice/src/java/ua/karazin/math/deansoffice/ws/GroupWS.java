/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.karazin.math.deansoffice.ws;

import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import ua.karazin.math.deansoffice.business.GroupLogic;
import ua.karazin.math.deansoffice.view.Group.GroupShortInfoView;
import ua.karazin.math.deansoffice.view.Group.GroupTreeView;
import ua.karazin.math.deansoffice.view.Student.ShortInfoStudentView;

/**
 *
 * @author Alyona
 */
@WebService(serviceName = "GroupWS")
@Stateless()
public class GroupWS {
    @EJB
    private GroupLogic ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "getGroupTree")
    public List<GroupTreeView> getGroupTree() {
        return ejbRef.getGroupTree();
    }

    @WebMethod(operationName = "getGroupListShortInfoStudent")
    public ShortInfoStudentView getGroupListShortInfoStudent(@WebParam(name = "id") Integer id) {
        return ejbRef.getGroupListShortInfoStudent(id);
    }

    @WebMethod(operationName = "getGroupShortInfo")
    public GroupShortInfoView getGroupShortInfo(@WebParam(name = "id") Integer id) {
        return ejbRef.getGroupShortInfo(id);
    }

    @WebMethod(operationName = "setGroupShortInfo")
    @Oneway
    public void setGroupShortInfo(@WebParam(name = "group") GroupShortInfoView group) {
        ejbRef.setGroupShortInfo(group);
    }
    
}
