/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.karazin.math.deansoffice.ws;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import ua.karazin.math.deansoffice.business.StudentLogic;
import ua.karazin.math.deansoffice.view.Student.FullInfoStudentView;
import ua.karazin.math.deansoffice.view.Student.InfoStudentView;
import ua.karazin.math.deansoffice.view.Student.ShortInfoStudentView;

/**
 *
 * @author Alyona
 */
@WebService(serviceName = "StudentWS")
@Stateless()
public class StudentWS {
    @EJB
    private StudentLogic ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "getShortInfoStudentList")
    public List<ShortInfoStudentView> getShortInfoStudentList(@WebParam(name = "lastName") String lastName) {
        return ejbRef.getShortInfoStudentList(lastName);
    }

    @WebMethod(operationName = "setShortInfoStudent")
    @Oneway
    public void setShortInfoStudent(@WebParam(name = "shortInfoStudent") ShortInfoStudentView shortInfoStudent) {
        ejbRef.setShortInfoStudent(shortInfoStudent);
    }

    @WebMethod(operationName = "getFullInfoStudent")
    public FullInfoStudentView getFullInfoStudent(@WebParam(name = "id") Integer id) {
        return ejbRef.getFullInfoStudent(id);
    }

    @WebMethod(operationName = "setFullInfoStudent")
    @Oneway
    public void setFullInfoStudent(@WebParam(name = "fullInfoStudent") FullInfoStudentView fullInfoStudent) {
        ejbRef.setFullInfoStudent(fullInfoStudent);
    }

    @WebMethod(operationName = "getInfoStudent")
    public InfoStudentView getInfoStudent(@WebParam(name = "id") Integer id) {
        return ejbRef.getInfoStudent(id);
    }

    @WebMethod(operationName = "setInfoStudent")
    @Oneway
    public void setInfoStudent(@WebParam(name = "infoStudent") InfoStudentView infoStudent) {
        ejbRef.setInfoStudent(infoStudent);
    }
    
}
