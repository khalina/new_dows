package ua.karazin.math.deansoffice.ui;
/** Localizable strings for {@link ua.karazin.math.deansoffice.ui}. */
@javax.annotation.Generated(value="org.netbeans.modules.openide.util.NbBundleProcessor")
class Bundle {
    /**
     * @return <i>Department</i>
     * @see DepartmentTopComponent
     */
    static String CTL_DepartmentAction() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "CTL_DepartmentAction");
    }
    /**
     * @return <i>Department Window</i>
     * @see DepartmentTopComponent
     */
    static String CTL_DepartmentTopComponent() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "CTL_DepartmentTopComponent");
    }
    /**
     * @return <i>Subject</i>
     * @see SubjectListTopComponent
     */
    static String CTL_SubjectAction() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "CTL_SubjectAction");
    }
    /**
     * @return <i>Subject Window</i>
     * @see SubjectListTopComponent
     */
    static String CTL_SubjectTopComponent() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "CTL_SubjectTopComponent");
    }
    /**
     * @return <i>Teacher</i>
     * @see TeacherTopComponent
     */
    static String CTL_TeacherAction() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "CTL_TeacherAction");
    }
    /**
     * @return <i>Teacher Window</i>
     * @see TeacherTopComponent
     */
    static String CTL_TeacherTopComponent() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "CTL_TeacherTopComponent");
    }
    /**
     * @return <i>TeacherTree</i>
     * @see TeacherTreeTopComponent
     */
    static String CTL_TeacherTreeAction() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "CTL_TeacherTreeAction");
    }
    /**
     * @return <i>TeacherTree Window</i>
     * @see TeacherTreeTopComponent
     */
    static String CTL_TeacherTreeTopComponent() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "CTL_TeacherTreeTopComponent");
    }
    /**
     * @return <i>This is a Department window</i>
     * @see DepartmentTopComponent
     */
    static String HINT_DepartmentTopComponent() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "HINT_DepartmentTopComponent");
    }
    /**
     * @return <i>This is a Subject window</i>
     * @see SubjectListTopComponent
     */
    static String HINT_SubjectTopComponent() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "HINT_SubjectTopComponent");
    }
    /**
     * @return <i>This is a Teacher window</i>
     * @see TeacherTopComponent
     */
    static String HINT_TeacherTopComponent() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "HINT_TeacherTopComponent");
    }
    /**
     * @return <i>This is a TeacherTree window</i>
     * @see TeacherTreeTopComponent
     */
    static String HINT_TeacherTreeTopComponent() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "HINT_TeacherTreeTopComponent");
    }
    private void Bundle() {}
}
