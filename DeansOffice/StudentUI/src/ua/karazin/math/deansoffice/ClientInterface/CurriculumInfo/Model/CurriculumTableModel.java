/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ua.karazin.math.deansoffice.ClientInterface.CurriculumInfo.Model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import ua.karazin.math.deansoffice.ws.CurriculumWS;
import ua.karazin.math.deansoffice.ws.CurriculumWS_Service;
import ua.karazin.math.deansoffice.ws.EducationPlanEntryView;
import ua.karazin.math.deansoffice.ws.StudentWS;
import ua.karazin.math.deansoffice.ws.StudentWS_Service;


/**
 *
 * @author DEN
 */
public class CurriculumTableModel extends AbstractTableModel {
    private String[] columnNames={"№ учебного плана", "Название", "Всего часов", "Номер семестра","Число аудит. часов", "Тип отчета", "Обязательность", "Код","Количество модулей"};
    private Object[][] data;
    
    private List<EducationPlanEntryView> educationPlanEntryViewList= new ArrayList();
    
     public void addElement(Long index)
    {
        
        CurriculumWS_Service service = new CurriculumWS_Service();
        CurriculumWS port = service.getCurriculumWSPort();
        educationPlanEntryViewList = port.getEducationPlanEntryList(index);
        int i=0;
        for(EducationPlanEntryView element : educationPlanEntryViewList){
            data[i][0] = element.getId();
            data[i][1] = element.getName();
           // data[i][2] = element.getHoursTotal();
          //  data[i][3] = element.getSemestr();
          //  data[i][4] = element.getHoursAuditor();
            data[i][5] = element.getReportType();
            data[i][6] = element.isIsMandatory();
            data[i][7] = element.getSCode();
          //  data[i][8] = element.getCountModules();
            i++;
        }
    }
    
        @Override
    public int getRowCount() {
        //throw new UnsupportedOperationException("Not supported yet.");
        return educationPlanEntryViewList.size();
    }

    @Override
    public int getColumnCount() {
        //throw new UnsupportedOperationException("Not supported yet.");
        return 9;
    }
    @Override
    public String getColumnName(int col) {
            return columnNames[col];
        }
    @Override
     public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }
 
    @Override
    public Object getValueAt(int row, int col) {
            return data[row][col];
       }
    
    @Override
     public boolean isCellEditable(int row, int col) {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
            if (row == 1) {
                return false;
            } else {
                return true;
            }
        }
    @Override
    public void setValueAt(Object value, int row, int col) {
//            if (DEBUG) {
//                System.out.println("Setting value at " + row + "," + col
//                                   + " to " + value
//                                   + " (an instance of "
//                                   + value.getClass() + ")");
//            }
 
            data[row][col] = value;
            fireTableCellUpdated(row, col);
            setElement(row);
 
//            if (DEBUG) {
//                System.out.println("New value of data:");
//                printDebugData();
//            }
        }
    private void setElement(int row){
    }
    
}
