/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.karazin.math.deansoffice.ClientInterface.StudentInfo.Model;

import java.util.Date;
import javax.swing.table.AbstractTableModel;
import ua.karazin.math.deansoffice.ws.FullInfoStudentView;
import ua.karazin.math.deansoffice.ws.StudentWS;
import ua.karazin.math.deansoffice.ws.StudentWS_Service;

/**
 *
 * @author Ира
 */


public class StudentTableModel extends AbstractTableModel{
    //String[] NameField = new String[] {"№ студенческого билета","№ идентификационный","Дата рождения","Паспорт","Год поступления","Предыдущее образование","№ аттестата","№ диплома бакалавра","№ диплома магистра/специалиста","Категория","Специальность","Кафедра","Специализация","Семейное положение","Сведения о родителях","Гражданство","Адрес прописки","e-mail","Именная стипендия","Благодарности","Выговоры","Не допущен к сессии по причинам","Тема квалификационной работы","Тема дипломной работы"};
    private String[] columnNames={"Название", "Данные о студентах"};
    private Object[][] data = {
                {"№ студенческого билета", new Integer(0)},
                {"№ идентификационный", new String()},
                {"Дата рождения", new Date()},
                {"Паспорт", new String()},
                {"Год поступления", new Integer(0)},
                {"Предыдущее образование", new String()},
                {"№ аттестата", new String()},
                {"№ диплома бакалавра", new String()},
                {"№ диплома магистра/специалиста", new String()},
                {"Категория",new String() },
                {"Специальность", new String()},
                {"Кафедра", new String()},
                {"Специализация",new String()},
                {"Семейное положение", new String()},
                {"Сведения о родителях", new String()},
                {"Гражданство", new String()},
                {"Адрес прописки", new String()},
                {"e-mail", new String()},
                {"Именная стипендия", new String()},
                {"Благодарности", new String()},
                {"Выговоры", new String()},
                {"Не допущен к сессии по причинам", new String()},
                {"Тема квалификационной работы", new String()},
                {"Тема дипломной работы", new String()}};
    private FullInfoStudentView FullInfoStudentView=new FullInfoStudentView();
   
    public void addElement(Integer index)
    {
        
        StudentWS_Service service = new StudentWS_Service();
        StudentWS port = service.getStudentWSPort();
        FullInfoStudentView=port.getFullInfoStudent(index);
        data[0][1]=FullInfoStudentView.getStudNumber();
        data[1][1]=FullInfoStudentView.getIDCCode();
        data[2][1]=FullInfoStudentView.getBirthday();
        data[3][1]=FullInfoStudentView.getPasport();  
        data[4][1]=new Integer(FullInfoStudentView.getFromYear());
        data[5][1]=FullInfoStudentView.getPreviousEducation();
       // data[6][1]=FullInfoStudentView.getNumberAtestat();//нужна пролверка является ли студент бакалавром
       // data[7][1]=FullInfoStudentView.getNumberDiplom();//нужна пролверка является ли студент бакалавром
       // data[8][1]=FullInfoStudentView.getNumberDiplom();//нужна пролверка является ли студент бакалавром
        data[9][1]=FullInfoStudentView.getNameCategory();
        data[10][1]=FullInfoStudentView.getNameSpecialization();
        //Кафедра  data[1][10]=FullInfoStudentView;
        data[11][1]=FullInfoStudentView.getNameSpecialization();
        data[12][1]=FullInfoStudentView.getMaritalStatus();
        data[13][1]=FullInfoStudentView.getPerentsInfo();
        data[15][1]=FullInfoStudentView.getNameCitizenship();
        data[16][1]=FullInfoStudentView.getRegistrationAdress();
        data[17][1]=FullInfoStudentView.getEMail();
        data[18][1]=FullInfoStudentView.getNameNominalGrant();
        data[19][1]=FullInfoStudentView.getHistoryRecordType();
        data[20][1]=FullInfoStudentView.getHistoryRecordType();
        data[21][1]=FullInfoStudentView.getHistoryRecordType();
        data [22][1]=FullInfoStudentView.getNameOfWork();
        data [23][1]=FullInfoStudentView.getNameOfWork();
    }

    @Override
    public int getRowCount() {
        //throw new UnsupportedOperationException("Not supported yet.");
        return data.length;
    }

    @Override
    public int getColumnCount() {
        //throw new UnsupportedOperationException("Not supported yet.");
        return columnNames.length;
    }
    @Override
    public String getColumnName(int col) {
            return columnNames[col];
        }
    @Override
     public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }
 
    @Override
    public Object getValueAt(int row, int col) {
            return data[row][col];
        }
    @Override
     public boolean isCellEditable(int row, int col) {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
            if (col < 1) {
                return false;
            } else {
                return true;
            }
        }
    @Override
    public void setValueAt(Object value, int row, int col) {
//            if (DEBUG) {
//                System.out.println("Setting value at " + row + "," + col
//                                   + " to " + value
//                                   + " (an instance of "
//                                   + value.getClass() + ")");
//            }
 
            data[row][col] = value;
            fireTableCellUpdated(row, col);
            setElement(row);
 
//            if (DEBUG) {
//                System.out.println("New value of data:");
//                printDebugData();
//            }
        }
    private void setElement(int row){
        switch(row){
            case 0:
                FullInfoStudentView.setStudNumber((Integer)data[0][1]);
                break;
            case 1:
                FullInfoStudentView.setIDCCode(data[1][1].toString()); 
          //  case 2:
                
        }
        StudentWS_Service service = new StudentWS_Service();
        StudentWS port = service.getStudentWSPort();
        port.setFullInfoStudent(FullInfoStudentView);
    }
//   @Override
//    public Object getValueAt(int rowIndex, int columnIndex) {
//        //throw new UnsupportedOperationException("Not supported yet.");
//        Object obj=new Object();
//      
//        if(rowIndex==1){
//        switch(columnIndex)
//        {
//            case 0:
//              obj=FullInfoStudentView.getStudNumber();
//                break;
//            case 1:
//              obj=FullInfoStudentView.getIDCCode(); 
//                break;
//            case 2:
//              obj=FullInfoStudentView.getBirthday();
//                break;
//            case 3:
//              obj=FullInfoStudentView.getPasport();
//                break;
//            case 4:
//              obj=FullInfoStudentView.getFromYear();
//                break;
//            case 5:
//              obj=FullInfoStudentView.getPreviousEducation();
//                break;
//            case 6:
//              obj=FullInfoStudentView.getNumberAtestat();
//                break;
//            default:
//                obj=null;
//        }
//        }
//        else
//        {
//        }
//        return obj;
//        
//    }
}
