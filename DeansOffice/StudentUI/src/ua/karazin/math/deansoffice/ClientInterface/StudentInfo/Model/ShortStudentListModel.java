/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.karazin.math.deansoffice.ClientInterface.StudentInfo.Model;

import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import ua.karazin.math.deansoffice.ws.ShortInfoStudentView;
import ua.karazin.math.deansoffice.ws.StudentWS;
import ua.karazin.math.deansoffice.ws.StudentWS_Service;

/**
 *
 * @author Alyona
 */
public class ShortStudentListModel extends AbstractListModel{

    private List<ShortInfoStudentView> ShortStudList= new ArrayList <>();
    @Override
    public int getSize() {
        return ShortStudList.size();
    }
    
    public void addElements(String str)
    {
        ShortStudList.clear();
        StudentWS_Service service = new StudentWS_Service();
        StudentWS port = service.getStudentWSPort();
        ShortStudList=port.getShortInfoStudentList(str);
        fireContentsChanged(this,0,ShortStudList.size());
        
        //List<ShortInfoStudentView> listInfo = port.findByFirstName("И");
        //List<ShortInfoStudentData> listStudent = new ArrayList<ShortInfoStudentData>();
        //for (ShortInfoStudentView student : listInfo) {
        //    ShortInfoStudentData info = new ShortInfoStudentData(student);
        //    listStudent.add(info);
        //  }
        //DefaultListModel listMod = new DefaultListModel();
        //for (ShortInfoStudentData student : listStudent) {
        //    listMod.addElement(student);
        //  }
        //ShortStudentList.setModel(listMod);
    }
    
    @Override
    public Object getElementAt(int index) {
        return ShortStudList.get(index).getLastName()+ " " +ShortStudList.get(index).getFirstName() + " " + ShortStudList.get(index).getSecondName();
    }
    
}
