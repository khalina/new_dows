/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.karazin.math.deansoffice.ClientInterface.StudentInfo.Model;

import javax.swing.table.AbstractTableModel;
//import ua.karazin.math.deansoffice.ws.GroupInfoView;
import ua.karazin.math.deansoffice.ws.GroupWS;
import ua.karazin.math.deansoffice.ws.GroupWS_Service;

/**
 *
 * @author Ира
 */
public class GroupStudTableModel extends AbstractTableModel{
    private String[] columnNames={"Название", "Данные о студентах"};
    private Object[][] data = {
                {"Фамилия",new String() },
                {"Имя", new String()},
                {"Отчество", new String()},
                {"№ зачетки", new String()},
                {"№ студенческого", new Integer(0)},
                {"№ читательского", new String()},
                {"Бюджет", new Integer(0)},
                {"Национальность", new String()},
                {"Уровень образования", new String()},
                {"Адрес",new String() },
                {"Телефон", new String()}};
   // private groupInfo = new GroupInfoView(); должен быть список студентов группы
  public void loadGroupStudInfo(Integer index)
   {
//        GroupWS_Service service = new GroupWS_Service();
//        GroupWS port = service.getGroupWSPort();
//        groupInfo=port.getGroupInfo(index);
//        data[1][0]=groupInfo.getFirstName();
//        data[1][1]=groupInfo.getSecondName();
//        data[1][2]=groupInfo.getLastName();
//        data[1][3]=groupInfo.getNumberRec();  
//        data[1][4]=new Integer(groupInfo.getNumberStud());
//        data[1][5]=groupInfo.getNumberLib();
//        data[1][6]=groupInfo.isIsBudjet();
//        data[1][7]=groupInfo.getCitizenship();
//        data[1][8]=groupInfo.getLevelEdu();
//        data[1][9]=groupInfo.getCurrentAdress();
//        data[1][10]=groupInfo.getPhone();
   }
    @Override
    public int getRowCount() {
        //throw new UnsupportedOperationException("Not supported yet.");
        return data.length;
    }

    @Override
    public int getColumnCount() {
        //throw new UnsupportedOperationException("Not supported yet.");
        return columnNames.length;
    }
    @Override
    public String getColumnName(int col) {
            return columnNames[col];
        }
    @Override
     public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }
 
    @Override
    public Object getValueAt(int row, int col) {
            return data[row][col];
        }
    @Override
     public boolean isCellEditable(int row, int col) {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
            if (col < 1) {
                return false;
            } else {
                return true;
            }
        }
    @Override
    public void setValueAt(Object value, int row, int col) { 
            data[row][col] = value;
            fireTableCellUpdated(row, col);
            setElement(row);
        }
    private void setElement(int row){
//        switch(row){
//            case 0:
//                groupInfo.setFirstName(data[1][0].toString());
//                break;
//            case 1:
//                groupInfo.setSecondName(data[1][1].toString()); 
//            case 2:
//                groupInfo.setLastName(data[1][2].toString());
//            case 3:
//                groupInfo.setNumberRec(data[1][3].toString());
//            case 4:
//                groupInfo.setNumberStud((Integer)data[1][4]);
//            case 5:
//                groupInfo.setNumberLib(data[1][5].toString());
//            case 6:
//                //groupInfo.setIsBudjet(data[1][6]);
//            case 7:
//                groupInfo.setCitizenship(data[1][7].toString());
//            case 8:
//                groupInfo.setLevelEdu(data[1][8].toString());
//            case 9:
//                groupInfo.setCurrentAdress(data[1][9].toString());
//            case 10:
//                groupInfo.setPhone(data[1][10].toString());
//        }
//        GroupWS_Service service = new GroupWS_Service();
//        GroupWS port = service.getGroupWSPort();
//        port.setGroupInfo(groupInfo);
    }
}
