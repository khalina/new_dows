/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.karazin.math.deansoffice.ClientInterface.StudentInfo;

import ua.karazin.math.deansoffice.ClientInterface.StudentInfo.Model.StudentTableModel;

/**
 *
 * @author Alyona
 */
public class FullInfoStudentPanel extends javax.swing.JPanel {

    /**
     * Creates new form FullInfoStudentPanel
     */
    public FullInfoStudentPanel() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        PropertyStudent = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        //StudentTableModel StudTableModel = new StudentTableModel();
        //PropertyStudent.setModel(StudTableModel);
        //StudTableModel.addElement(1);
        PropertyStudent.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(PropertyStudent);

        add(jScrollPane1, java.awt.BorderLayout.PAGE_START);
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable PropertyStudent;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
