/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.karazin.math.deansoffice.ui.TeacherTree;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;

/**
 * Класс, содержащий главный узел для дерева преподавателей
 * @author Alyona
 */
public class DepartmentRootNode extends AbstractNode {
    
    /** Конструктор, котрый создает сам узел, устанавливает для него имя "Кафедры"
     * и вызывает фабрику DepartmentChildFactory для создания дочерних узлов DepartmentNode 
     * 
     */

     public DepartmentRootNode() {
        super (Children.create(new DepartmentChildFactory(), true));
        setDisplayName ("Кафедры");
      //  setSheet(null);
    }
}
