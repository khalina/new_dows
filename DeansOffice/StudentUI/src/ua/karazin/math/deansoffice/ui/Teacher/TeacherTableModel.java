/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.karazin.math.deansoffice.ui.Teacher;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import ua.karazin.math.deansoffice.ws.ShortSubjectView;
import ua.karazin.math.deansoffice.ws.TeacherView;
import ua.karazin.math.deansoffice.ws.TeacherWS;
import ua.karazin.math.deansoffice.ws.TeacherWS_Service;

/**
 * Модель таблицы, содержащей полную информацию о выбранном преподавателе
 * @author Helen
 */


public class TeacherTableModel extends AbstractTableModel{
    private String[] columnNames={"Название", "Данные о преподавателях"};
    private Object[][] data = {
                {"Фамилия", ""},
                {"Имя", ""},
                {"Отчество", ""},
                {"Кафедра", ""},
                {"Должность", ""},
                {"Ученая степень", ""},
                {"Адрес", ""},
                {"Телефон", ""},
                {"№ идентификационный", ""},
                {"Паспорт серия", ""},
                {"Паспорт номер", ""},
                {"Дата рождения", ""},
                {"Что закончил", ""},
                {"В каком году", ""}
    };
    private TeacherView teacherView; //объект класса, содержащего полную информацию о преподавателе

    /**
     * Метод получает данные преподавателя, с указаным кодом, через соответствующую веб-службу 
     * и заполняет модель таблицы
     * @param idTeacher код преподавателя
     */
    public void loadTeacherInfo(TeacherView sourseteacherView)
    {
        teacherView = sourseteacherView;//port.getTeacher(idTeacher);
        data[0][1]=sourseteacherView.getLastName();
        data[1][1]=sourseteacherView.getFirstName();
        data[2][1]=sourseteacherView.getSecondName();
        data[3][1]= sourseteacherView.getNameDepartment();
        data[4][1]= sourseteacherView.getPost();
        data[5][1]= sourseteacherView.getSciDegree();
        data[6][1]= sourseteacherView.getAddress();
        data[7][1]= sourseteacherView.getPhone();
        data[8][1]= sourseteacherView.getIDCode();
        data[9][1]= sourseteacherView.getSeries();
        data[10][1]= sourseteacherView.getNumber();
        data[11][1]= sourseteacherView.getBirthday();
        data[12][1]= sourseteacherView.getEducation();
        data[13][1]= sourseteacherView.getEducationYear();
       
    }
   
    @Override
    public int getRowCount() {
        return data.length;
    }

    @Override
    public int getColumnCount() {
       return columnNames.length;
    }
    @Override
    public String getColumnName(int col) {
            return columnNames[col];
        }
//    @Override
//     public Class<?> getColumnClass(int c) {
//        return null;
////            return getValueAt(0, c).getClass();
//        }

    @Override
    public Object getValueAt(int row, int col) {
            return data[row][col];
        }
    @Override
     public boolean isCellEditable(int row, int col) {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
            if (col < 1) {
                return false;
            } else {
                return true;
            }
        }
    @Override
    public void setValueAt(Object value, int row, int col) {

            data[row][col] = value;
            fireTableCellUpdated(row, col);
            setElement(row);

        }
    private void setElement(int row){
        switch(row)
        {
            case 0:
                //FullInfoStudentView.setStudNumber((Integer)data[0][1]);
               // break;

        }
    }
}
