/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.karazin.math.deansoffice.ui.TeacherTree;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import org.openide.ErrorManager;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import ua.karazin.math.deansoffice.ui.DepartmentTopComponent;
import ua.karazin.math.deansoffice.ws.DepartmentView;


/**
 * Узел дерева на основе объекта класса DepartmentView, который содержит 
 * информацию о кафедре
 * @author Alyona
 */
public class DepartmentNode extends AbstractNode {
/**
 * 
 * @param obj создает узел дерева типа DepartmentNode, на основе объекта DepartmentView
 * и вызывает фабрику TeacherChildFactory для создания дочерних узлов типа TeacherNode
 */
    public DepartmentNode(DepartmentView obj) {
        super(Children.create(new TeacherChildFactory(obj.getIdDepartment()), true), Lookups.singleton(obj));
        setDisplayName(obj.getName());
    }
/**
 * создает набор свойств объекта DepartmentView, для отображении в окне Свойства
 * @return 
 */
    @Override
    protected Sheet createSheet() {

        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        DepartmentView obj = getLookup().lookup(DepartmentView.class);

        try {

            Property indexProp = new PropertySupport.Reflection(obj, Long.class, "getIdDepartment", null);
            Property nameProp = new PropertySupport.Reflection(obj, String.class, "getName", "setName");
           // Property headProp = new PropertySupport.Reflection(obj, String.class,"getNameHead",null);
            // Property ingenieerProp = new PropertySupport.Reflection(obj, String.class,"getIngenieer",null);
            Property phoneProp = new PropertySupport.Reflection(obj, String.class,"getPhone",null);
            Property auditoryProp = new PropertySupport.Reflection(obj, String.class,"getAuditory",null);
//     
            indexProp.setName("index");
            nameProp.setName("Название");
           // headProp.setName("Заведующий кафедрой");
            phoneProp.setName("Телефон");
            auditoryProp.setName("Аудитория");
            

            set.put(indexProp);
            set.put(nameProp);
         //   set.put(headProp);
            set.put(phoneProp);
            set.put(auditoryProp);
            

        } catch (NoSuchMethodException ex) {
            ErrorManager.getDefault();
        }

        sheet.put(set);
        return sheet;

    }
//     public void actionPerformed(ActionEvent e) {
//        DepartmentView taskMgr =   Lookup.getDefault().lookup(DepartmentView.class);
//        if (null != taskMgr) {
//        //Task task = taskMgr.createTask();
//        DepartmentTopComponent win= new DepartmentTopComponent();//(DepartmentView);
//        win.open();
//        win.requestActive();
//}
////     }

    /**
     * возвращает набор событий для узла DepartmentNode 
     * @param popup
     * @return набор событий для узла DepartmentNode
     */
    @Override
    public Action[] getActions(boolean popup) {
        return new Action[]{new MyAction()};
    }

    /**
     * внутренний класс для события
     */
    private class MyAction extends AbstractAction {

        public MyAction() {
            putValue(NAME, "Do Something");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            DepartmentView obj = getLookup().lookup(DepartmentView.class);
            //JOptionPane.showMessageDialog(null, "Hello from " + obj);
           
        }
    }
}
