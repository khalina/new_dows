/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.karazin.math.deansoffice.ui.TeacherTree;

import org.openide.ErrorManager;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.lookup.Lookups;
import ua.karazin.math.deansoffice.ws.TeacherView;

/**
 * Узел дерева, содержащий данные о преподавателе
 * @author Alyona
 */
public class TeacherNode extends AbstractNode  {
    public TeacherNode(TeacherView obj) {
        super (Children.LEAF, Lookups.singleton(obj));
        setDisplayName (obj.getLastName()+" "+obj.getFirstName()+ " "+obj.getSecondName());
    }
    
    @Override
    protected Sheet createSheet() {

        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        TeacherView obj = getLookup().lookup(TeacherView.class);

        try {

            Property indexProp = new PropertySupport.Reflection(obj, Long.class, "getIdTeacher", null);
            Property lastNameProp = new PropertySupport.Reflection(obj, String.class, "getLastName", null);
            Property firstNameProp = new PropertySupport.Reflection(obj, String.class, "getFirstName", null);
            Property secondNameProp = new PropertySupport.Reflection(obj, String.class, "getSecondName", null);
          //  Property postProp;
//            if (obj.getPost()==null) {  
//                postProp = new PropertySupport.Reflection(obj,String.class,"");
//            }
//            else {
             //   postProp = new PropertySupport.Reflection(obj, String.class, "getPost", null);
           // }
            
            indexProp.setName("index");
            lastNameProp.setName("Фамилия");
            firstNameProp.setName("Имя");
            secondNameProp.setName("Отчество");
           // postProp.setName("Должность");

            set.put(indexProp);
            set.put(lastNameProp);
            set.put(firstNameProp);
            set.put(secondNameProp);
          //  set.put(postProp);

        } catch (NoSuchMethodException ex) {
            ErrorManager.getDefault();
        }

        sheet.put(set);
        return sheet;

    }

}
