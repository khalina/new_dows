/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.karazin.math.deansoffice.ui.TeacherTree;

import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import ua.karazin.math.deansoffice.ws.TeacherView;
import ua.karazin.math.deansoffice.ws.TeacherWS;
import ua.karazin.math.deansoffice.ws.TeacherWS_Service;

/**
 * Класс-фабрика для создание узлов TeacherNode
 * @author Alyona
 */
public class TeacherChildFactory extends ChildFactory<TeacherView>{
   
    /**
     * код кафедры
     */
    private long IdDepartment;
    
    public TeacherChildFactory(){
          }
    
     public TeacherChildFactory(long Id){
         IdDepartment=Id;
    }
     
    @Override
    protected boolean createKeys(List<TeacherView> list) {
        TeacherWS_Service service = new TeacherWS_Service();
        TeacherWS port = service.getTeacherWSPort();
        list.addAll(port.getTeacherDepartmentList(IdDepartment));
        return true;
    }
    
     @Override
    protected Node createNodeForKey(TeacherView key) {
        return new TeacherNode(key);
    }
    
}
