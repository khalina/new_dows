/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.karazin.math.deansoffice.ui.TeacherTree;

import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import ua.karazin.math.deansoffice.ws.DepartmentView;
import ua.karazin.math.deansoffice.ws.TeacherWS;
import ua.karazin.math.deansoffice.ws.TeacherWS_Service;

/**
 * Класс-фабрика для создание узлов DepartmentNode
 * @author Alyona
 */
public class DepartmentChildFactory extends ChildFactory<DepartmentView> {
    public DepartmentChildFactory() {
    }
 
    /**
     * Заполняет список toPopulate, получая данные от веб-сервиса TeacherWS.
     * На основе этого списка формируются узлы DepartmentNode
     * @param toPopulate список объектов класса DepartmentView, которые 
     * размещаются в узле DepartmentNode
     * @return 
     */

    @Override
    protected boolean createKeys(List<DepartmentView> toPopulate) {
        //обращается к соответствующей веб-службе и получает данные о преподавателях
        TeacherWS_Service service = new TeacherWS_Service();
        TeacherWS port = service.getTeacherWSPort();
         List<DepartmentView> departmentList = port.getDepartmentList();
         if (departmentList!=null)
        //заполняет поле класса полученными данными
         {
             toPopulate.addAll(departmentList);
             return true;
         }
         else {
            return false;
         } 
    }
    
    /**
     * На основе параметра key формирует новый узел DepartmentNode
     * @param key содержит объект класса DepartmentView
     * @return  новый узел DepartmentNode
     */
    @Override
    protected Node createNodeForKey(DepartmentView key) {
      
        return new DepartmentNode(key);
}
    
}
